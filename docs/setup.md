# Setup Documentation
**Set alias for python:**
-sudo vim ~/.bashrc
Add the following line-
alias python=/usr/bin/python2.7

**Install Virtualenvwrapper:**

-sudo pip install virtualenvwrapper
-sudo vim ~/.bashrc

Add the following lines to bashrc:

export WORKON_HOME=$HOME/.virtualenvs
export PROJECT_HOME=$HOME/Devel
VIRTUALENVWRAPPER_PYTHON=/usr/bin/python2.7

**Using virtualenvwrapper:**
**workon -** To see the list of virtaul envs
**mkvirtualenv -** To create a new virtual env
**workon name -** to use a env

**Set up SSH key for bitbucket:**

Generate key-
$ssh-keygen
will ask for location to save the key. Press enter for default.
You’ll have two files in the location - id_rsa  & id_rsa.pub

Adding key to the ssh agent - 
$ eval $(ssh-agent)
$ ssh-add ~/.ssh/id_rsa

Add the public key to bitbucket:
Go to settings in bitbucket.
Add contents of id_rsa.pub to the add key text.

**Install MongoDb:**

$wget -qO - https://www.mongodb.org/static/pgp/server-4.4.asc | sudo apt-key add -
$echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu focal/mongodb-org/4.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.4.list
$sudo apt-get update
Install the latest version of mongo:
$sudo apt-get install -y mongodb-org
MongoDB will start following a system reboot by issuing the following command:
$sudo systemctl enable mongod

**Install Robo3t:**
Install robo3t using Ubuntu Software

**Project Setup(Bitbucket):**

Clone the project by copying the SSH command.
Go to the directory and paste the command.
Create a virtual env for the backend project.
Make a develop branch:
$git checkout -b develop
Initialise git flow:
$git flow init

**Backend Project Settings:**
Create LOCAL_SETTINGS and copy the contents of the settings.py file in them.
Go into the virtual env for the specific project and run the following to install the dependencies 
$pip install -r requirement.txt

For service_scraper:
Create requirements.txt file and paste the following:

    attrs==19.3.0
    Automat==0.8.0
    backports.functools-lru-cache==1.6.1
    beautifulsoup4==4.8.2
    boto3==1.11.9
    botocore==1.14.9
    certifi==2019.11.28
    cffi==1.13.2
    chardet==3.0.4
    constantly==15.1.0
    cryptography==2.8
    cssselect==1.1.0
    dateparser==0.7.2
    docutils==0.15.2
    enum34==1.1.6
    fake-useragent==0.1.11
    functools32==3.2.3.post2
    futures==3.3.0
    hyperlink==19.0.0
    idna==2.8
    incremental==17.5.0
    ipaddress==1.0.23
    jmespath==0.9.4
    lxml==4.4.2
    numpy==1.16.6
    pandas==0.24.2
    parsel==1.5.2
    Protego==0.1.16
    pyasn1==0.4.8
    pyasn1-modules==0.2.8
    pycparser==2.19
    PyDispatcher==2.0.5
    PyHamcrest==1.10.1
    pymongo==3.10.1
    pyOpenSSL==19.1.0
    python-dateutil==2.8.1
    pytz==2019.3
    PyYAML==5.3
    queuelib==1.5.0
    redis==3.3.11
    regex==2020.1.8
    requests==2.22.0
    s3transfer==0.3.2
    Scrapy==1.5.1
    scrapyd==1.2.1
    scrapyd-client==1.2.0a1
    service-identity==18.1.0
    six==1.14.0
    soupsieve==1.9.5
    Twisted==19.10.0
    typing==3.7.4.3
    tzlocal==2.0.0
    ua-parser==0.8.0
    urllib3==1.25.8
    user-agents==2.0
    w3lib==1.21.0
    zope.interface==4.7.1


For service_data_api create a requirement file and paste the following:

    amqp==2.2.2
    APScheduler==3.3.1
    arrow==0.14.2
    asn1crypto==0.24.0
    backports.functools-lru-cache==1.5
    BeautifulSoup==3.2.1
    beautifulsoup4==4.6.3
    billiard==3.5.0.3
    boto==2.48.0
    boto3==1.10.28
    botocore==1.13.28
    celery==4.1.0
    certifi==2017.7.27.1
    cffi==1.11.0
    chardet==3.0.4
    chromedriver==2.24.1
    click==6.7
    croniter==0.3.20
    cryptography==3.2.1
    dateparser==0.6.0
    decorator==4.4.2
    docutils==0.14
    enum34==1.1.6
    fake-useragent==0.1.11
    Flask==1.0.2
    Flask-Cors==3.0.3
    Flask-Login==0.4.1
    fpdf==1.7.2
    funcsigs==1.0.2
    futures==3.1.1
    graypy==0.2.14
    httplib2==0.10.3
    idna==2.6
    ipaddress==1.0.18
    itsdangerous==0.24
    Jinja2==2.11.2
    jmespath==0.9.3
    jwt==0.5.2
    kafka-python==1.4.6
    kombu==4.1.0
    lxml==4.0.0
    MarkupSafe==1.0
    oauth2==1.9.0.post1
    Paste==3.5.0
    pyasn1==0.3.6
    pycparser==2.18
    pycryptodome==3.9.9
    PyJWT==1.5.3
    pymongo==3.5.1
    pyOpenSSL==19.1.0
    pypng==0.0.20
    PyQRCode==1.2.1
    pysaml2==4.0.3
    python-dateutil==2.6.1
    pytz==2017.2
    redis==2.10.6
    regex==2017.9.23
    repoze.who==2.4
    requests==2.18.4
    rq==0.7.0
    rq-scheduler==0.8.2
    ruamel.ordereddict==0.4.13
    ruamel.yaml==0.15.34
    s3transfer==0.2.1
    selenium==3.141.0
    six==1.11.0
    typing==3.5.3.0
    tzlocal==1.4
    urllib3==1.22
    vine==1.1.4
    WebOb==1.8.6
    Werkzeug==0.14.1
    XlsxWriter==1.0.4
    xlwt==1.3.0
    xmltodict==0.11.0
    zope.interface==5.2.0

**Install nvm:**

$sudo apt update
$sudo apt install nodejs - install node if not installed 
$curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.37.2/install.sh | bash
$nvm list-remote (to see all the versions)
$nvm install v15.6.0 
$nvm alias default v15.6.0

**Install nginx:**

$sudo apt update
$sudo apt install nginx

$sudo apt-get install python-dev
$sudo apt-get install libffi-dev


